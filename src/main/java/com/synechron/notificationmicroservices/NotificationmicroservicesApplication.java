package com.synechron.notificationmicroservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationmicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificationmicroservicesApplication.class, args);
	}

}
